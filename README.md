# spherical_harmonics_and_legendre_polynomials

Using scipy derivative to define the Legendre polynomials  
plot of the Legendre polynomials:  
![alt text](images/Legendre_polynomials.png "one order")  

![alt text](images/Legendre_polynomials_1.png "several orders")  

Then define the sphericals harmonics:  
![alt text](images/spherical_harmonics.png "l=4, m=0")  


Python librairie use for the plot style : seaborn

## Usage
```bash
python main.py
```
