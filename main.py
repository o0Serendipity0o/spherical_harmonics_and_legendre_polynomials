import scipy
from matplotlib import pyplot as plt
import numpy as np
import seaborn as sns
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.mplot3d import Axes3D


sns.set(style="whitegrid", palette='deep')
sns.set_context("paper")


def differential_legendre(x):
    d = (x**2 - 1)**el
    return d


def factor_legendre(x, m, el):
    P = pow(-1, m) / (pow(2, el) * np.math.factorial(el)) * pow(1 - x**2, m / 2)
    return P


def polynomial_legendre(x, dx, n, order, m, el):
    polynome = scipy.misc.derivative(differential_legendre, x, dx=dx, n=n,
                                     order=order) * factor_legendre(x, m, el)
    return polynome


def spherical_harmonic(dx, n, order, m, el, theta, phi):
    A = np.sqrt((2 * el + 1 / (4 * np.pi)) * (np.math.factorial(el - m) / np.math.factorial(el + m)))
    C = np.exp(1j * m * phi)
    spherical_h = A * C * polynomial_legendre(np.cos(theta), dx, n, order, m, el)
    return spherical_h


def polar_cartesian(theta, phi):
    x = np.sin(phi) * np.cos(theta)
    y = np.sin(phi) * np.sin(theta)
    z = np.cos(phi)
    return x, y, z


def plot_legendre(x, dx, n, order, m, el):
    polynome = polynomial_legendre(x, dx, n, order, m, el)
    plt.plot(x, polynome, label='$ l={}, m={}$'.format(el, m))
    plt.title("Legendre Polynomials", fontsize=14)
    plt.legend()


def plot_spheric_harm(dx, n, order, m, el, Phi, Theta):
    spherical = spherical_harmonic(dx, n, order, m, el, Phi, Theta)
    if m < 0:
        spherical = spherical.imag
    elif m > 0:
        spherical = spherical.real

    x, y, z = polar_cartesian(Theta, Phi)
    Yx = np.abs(spherical) * x
    Yy = np.abs(spherical) * y
    Yz = np.abs(spherical) * z

    fig = plt.figure(figsize=plt.figaspect(1.))
    ax = fig.add_subplot(projection='3d')

    cmap = plt.cm.ScalarMappable(cmap=sns.diverging_palette(220, 20, as_cmap=True))
    ax.plot_surface(Yx, Yy, Yz, facecolors=cmap.to_rgba(spherical.real), rstride=2, cstride=2)

    ax.set_title(r'$Y_l^m(\theta,\varphi): l={}, m={}$'.format(el, m), fontsize=14)
    ax.axis('off')
    plt.show()


def example1():
    deltax = 0.03
    phi = np.arange(0, np.pi, deltax)
    theta = np.arange(0, 2 * np.pi, deltax)
    # Create a 2-D meshgrid of (theta, phi) angles.
    Theta, Phi = np.meshgrid(theta, phi)

    # l looks like another letter or number note a good choice for a varialble
    global el
    el = 4
    m = 0
    # calcul of the different parameter to use the scipy derivative fuction :
    # Need it to have the derivation good
    dx = 0.003
    # correspond to n in the derivative function
    level = m + el
    # belong to the derivate function have to be an odd number bigger than n
    if (level % 2) == 0:
        order = level + 1
    else:
        order = level + 2

    plot_spheric_harm(dx, level, order, m, el, Phi, Theta)
    plt.show()


def example2():
    xmin = -5
    xmax = 20
    res = 1000
    deltax = (xmax - xmin) / res
    x = np.arange(xmin, xmax, deltax)
    # l looks like another letter or number note a good choice for a varialble
    global el
    m = 0
    el = 4
    # calcul of the different parameter to use the scipy derivative fuction :
    # Need it to have the derivation good
    dx = 0.003
    # correspond to n in the derivative function
    level = m + el
    # belong to the derivate function have to be an odd number bigger than n
    if (level % 2) == 0:
        order = level + 1
    else:
        order = level + 2

    plot_legendre(x, dx, level, order, m, el)
    plt.show()


def example3():
    xmin = -1
    xmax = 1
    res = 1000
    deltax = (xmax - xmin) / res
    x = np.arange(xmin, xmax, deltax)
    # l looks like another letter or number note a good choice for a varialble
    global el
    m = 0
    # calcul of the different parameter to use the scipy derivative fuction :
    # Need it to have the derivation good
    dx = 0.003
    # correspond to n in the derivative function
    for el in range(9):
        level = m + el
        # belong to the derivate function have to be an odd number bigger than n
        if (level % 2) == 0:
            order = level + 1
        else:
            order = level + 2

            plot_legendre(x, dx, level, order, m, el)
    plt.show()


if __name__ == '__main__':
    # example1()
    # example2()
    example3()
